﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : GameComponent 
{        
    private Vector3 playerVelocity = Vector2.zero;
    private bool isFlap = false;
    private Vector2 velocity;
    
    public void StartNewGame()
    {
        GameCore.gameModel.isPlaying = false;
        GameCore.gameModel.isFirstTap = false;  
        GameCore.gameView.playerView.playerMode = PlayerMode.Beside;
        switch (Random.Range(0, 3))
        {
            case 0:
            startLines_1();
            break;
            case 1:
            startLines_2();
            break;
            case 2:
            startLines_3();
            break;
        }
    }
    
    public void MouseClick ()
    {
        if (Input.GetMouseButtonDown(0))
        {      
            if (GameCore.gameModel.isFirstTap == false)
            {
                GameCore.gameModel.isFirstTap = true;
                GameCore.gameModel.isPlaying = true;                
            }                                    
            isFlap = true;
        }       
    }
    
    public void Bounce()
    {
        if(GameCore.gameModel.isPlaying == true)
        {
            playerVelocity += GameCore.gameModel.gravity * Time.deltaTime;
            if(isFlap == true)
            {
                isFlap = false;
                playerVelocity += GameCore.gameModel.flapVelocity;
            }
            playerVelocity = Vector2.ClampMagnitude(playerVelocity, GameCore.gameModel.flapSpeed);
            GameCore.gameView.playerView._transform.position += playerVelocity * Time.deltaTime;   
        }           
    }
    
    public IEnumerator DetectBounceFail()
    {
        Vector2 screenPosition = Camera.main.WorldToScreenPoint(GameCore.gameView.playerView._transform.position);
        if ( screenPosition.y < 90)   
        {
            print("FAIL");
        }
        yield return new WaitForSeconds(1.0f);
    }
    
    public void CameraInit()
    {
        Vector3 bordersPos = Camera.main.ScreenToWorldPoint( new Vector3 (Screen.width, Screen.height, 0.0f));
        GameCore.gameView.cameraView.topPos = bordersPos.y;
    }
    
    public void CameraFollow ()
    {
        Transform cameraTransform = GameCore.gameView.cameraView._transform;
        Transform playerTransform = GameCore.gameView.playerView._transform;
        if(cameraTransform.position.y < playerTransform.position.y)
        {        
            float posY = Mathf.SmoothDamp(cameraTransform.position.y, playerTransform.position.y, ref velocity.y, 0.5f);    
            cameraTransform.position = new Vector3(cameraTransform.position.x, posY, cameraTransform.position.z);             
        }        
    }
    
    public void LoadLinesToPool()
    {
        GameCore.gameModel.LinesPool = new ObjectPool("Lines");
        GameCore.gameModel.LinesPool.LoadObjects(GameCore.gameModel.numberOfLineClones);
        for(int i = 0; i < GameCore.gameModel.LinesPool.Count; i++)
        {
            GameObject temp = GameCore.gameModel.LinesPool.GetPooledObjectAt(i);
            temp.transform.SetParent(GameCore.gameView._transform);
        }            
    }
    
    public void AddDrabLine()
    {
        switch (Random.Range(0, 2))
        {
            case 0:
                AddDrabLineToRight(0.0f);
            break;
            case 1:
                AddDrabLineToLeft(0.0f);
            break;
        }
    }
    
    public void AddDrabLineToRight(float xPos)
    {
        GameObject temp = GameCore.gameModel.LinesPool.GetPooledObjectSegment(2 * GameCore.gameModel.numberOfLineClones, 3 * GameCore.gameModel.numberOfLineClones);
        int yPos = GameCore.gameModel.currLineHeight + GameCore.gameModel.lineHeight;
        GameCore.gameModel.currLineHeight += GameCore.gameModel.lineHeight;
        temp.transform.position = new Vector3(xPos, yPos, 0.0f);
        temp.tag = "LineToRight";
        temp.SetActive(true);
    }
    
    public void AddDrabLineToLeft(float xPos)
    {
        GameObject temp = GameCore.gameModel.LinesPool.GetPooledObjectSegment(2 * GameCore.gameModel.numberOfLineClones, 3 * GameCore.gameModel.numberOfLineClones);
        int yPos = GameCore.gameModel.currLineHeight + GameCore.gameModel.lineHeight;
        GameCore.gameModel.currLineHeight += GameCore.gameModel.lineHeight;
        temp.transform.position = new Vector3(xPos, yPos, 0.0f);
        temp.tag = "LineToLeft";
        temp.SetActive(true);
    }
    
    public void AddDrabLineIdle()
    {
        GameObject temp = GameCore.gameModel.LinesPool.GetPooledObjectSegment(3 * GameCore.gameModel.numberOfLineClones, 4 * GameCore.gameModel.numberOfLineClones);
        int yPos = GameCore.gameModel.currLineHeight + GameCore.gameModel.lineHeight;
        GameCore.gameModel.currLineHeight += GameCore.gameModel.lineHeight;
        temp.transform.position = new Vector3(0.0f, yPos, 0.0f);
        temp.SetActive(true);
    }
    
    public void AddColorLine()
    {
        switch (Random.Range(0, 2))
        {
            case 0:
                AddColorLineToRight(0.0f);
            break;
            case 1:
                AddColorLineToLeft(0.0f);
            break;
        }
    }
    
    public void AddColorLineToRight(float xPos)
    {
        GameObject temp = GameCore.gameModel.LinesPool.GetPooledObjectSegment(0, GameCore.gameModel.numberOfLineClones);
        int yPos = GameCore.gameModel.currLineHeight + GameCore.gameModel.lineHeight;
        GameCore.gameModel.currLineHeight += GameCore.gameModel.lineHeight;
        temp.transform.position = new Vector3(xPos, yPos, 0.0f);
        temp.tag = "LineToRight";
        LineColor lineColorComponent = temp.GetComponent<LineColor>();
        lineColorComponent.colors = SetLineColor(lineColorComponent.elements, null);
        temp.SetActive(true);
    }
    
    public void AddColorLineToLeft(float xPos)
    {
        GameObject temp = GameCore.gameModel.LinesPool.GetPooledObjectSegment(0, GameCore.gameModel.numberOfLineClones);
        int yPos = GameCore.gameModel.currLineHeight + GameCore.gameModel.lineHeight;
        GameCore.gameModel.currLineHeight += GameCore.gameModel.lineHeight;
        temp.transform.position = new Vector3(xPos, yPos, 0.0f);
        temp.tag = "LineToLeft";
        LineColor lineColorComponent = temp.GetComponent<LineColor>();
        lineColorComponent.colors = SetLineColor(lineColorComponent.elements, null);
        temp.SetActive(true);
    }
    
    public void AddColorLineIdle()
    {
        GameObject temp = GameCore.gameModel.LinesPool.GetPooledObjectSegment(GameCore.gameModel.numberOfLineClones, 2 * GameCore.gameModel.numberOfLineClones);
        int yPos = GameCore.gameModel.currLineHeight + GameCore.gameModel.lineHeight;
        GameCore.gameModel.currLineHeight += GameCore.gameModel.lineHeight;
        temp.transform.position = new Vector3(0.0f, yPos, 0.0f);
        LineColorIdle lineColorComponent = temp.GetComponent<LineColorIdle>();
        lineColorComponent.colors = SetLineColorIdle(lineColorComponent.elements);
        temp.SetActive(true);
    }
    
    public void MoveLine(Transform lineTransform)
    {
        if(lineTransform.CompareTag("LineToRight"))
        {
            lineTransform.Translate(Vector3.right * GameCore.gameModel.lineSpeed * Time.deltaTime);
        }
        else
        {
            lineTransform.Translate(Vector3.left * GameCore.gameModel.lineSpeed * Time.deltaTime);
        }               
    } 
    
    public bool SupportDrabLine(Transform lineTransform)
    {       
        if(lineTransform.CompareTag("LineToRight") && lineTransform.position.x >= 110.0f)
        {
            GameObject temp = GameCore.gameModel.LinesPool.GetPooledObjectSegment(2 * GameCore.gameModel.numberOfLineClones, 3 * GameCore.gameModel.numberOfLineClones);
            temp.transform.position = new Vector3(lineTransform.position.x - 340, lineTransform.position.y, 0.0f);
            temp.tag = "LineToRight";
            temp.SetActive(true);
            return true;
        }
        else if(lineTransform.CompareTag("LineToLeft") && lineTransform.position.x <= -110.0f)
        {
            GameObject temp = GameCore.gameModel.LinesPool.GetPooledObjectSegment(2 * GameCore.gameModel.numberOfLineClones, 3 * GameCore.gameModel.numberOfLineClones);
            temp.transform.position = new Vector3(lineTransform.position.x + 340, lineTransform.position.y, 0.0f);
            temp.tag = "LineToLeft";
            temp.SetActive(true);
            return true;
        }                 
        return false;              
    } 
    
    public bool SupportColorLine(Transform lineTransform, Color[] colors)
    {     
        if(lineTransform.CompareTag("LineToRight") && lineTransform.position.x >= 67.0f)
        {
            GameObject temp = GameCore.gameModel.LinesPool.GetPooledObjectSegment(0, GameCore.gameModel.numberOfLineClones);
            temp.transform.position = new Vector3(lineTransform.position.x - 195.7f, lineTransform.position.y, 0.0f);
            temp.tag = "LineToRight";
            LineColor tempLineColorComponent = temp.GetComponent<LineColor>();
            tempLineColorComponent.colors = SetLineColor(tempLineColorComponent.elements, colors);            
            temp.SetActive(true);
            return true;
        }
        else if(lineTransform.CompareTag("LineToLeft") && lineTransform.position.x <= -67.0f)
        {
            GameObject temp = GameCore.gameModel.LinesPool.GetPooledObjectSegment(0, GameCore.gameModel.numberOfLineClones);
            temp.transform.position = new Vector3(lineTransform.position.x + 195.7f, lineTransform.position.y, 0.0f);
            temp.tag = "LineToLeft";
            LineColor tempLineColorComponent = temp.GetComponent<LineColor>();
            tempLineColorComponent.colors = SetLineColor(tempLineColorComponent.elements, colors);
            temp.SetActive(true);
            return true;
        }
        return false;                
    } 
    
    public bool IsLineVisible(Transform lineTransform)
    {
        if(GameCore.gameView.cameraView._transform.position.y + GameCore.gameView.cameraView.topPos + 2.0f >= lineTransform.position.y)
            return true;
        else return false;
    }
    
    public void DeactivationLine(Transform lineTransform)
    {
        if(lineTransform.position.y <= GameCore.gameView.cameraView._transform.position.y - GameCore.gameView.cameraView.topPos)
            GameCore.gameModel.LinesPool.DeactivateObject(lineTransform.gameObject);               
        else if(lineTransform.position.x >= 200.0f || lineTransform.position.x <= -200.0f)
            GameCore.gameModel.LinesPool.DeactivateObject(lineTransform.gameObject);
    }  
    
    public void LineControl()
    {                       
        if(GameCore.gameView.cameraView._transform.position.y + GameCore.gameView.cameraView.topPos >= GameCore.gameModel.currLineHeight)
        {
            GameCore.gameModel.modeCount++;
            
            switch (GameCore.gameModel.modeCount)
            {
                case 1:
                    GameCore.gameModel.lineHeight = 15; 
                    GameCore.gameModel.linesProbability = new float[3] { 0.4f, 0.4f, 0.2f};
                    AddLines(5);
                    break;                
                case 2:
                   // GameCore.gameView.playerView.playerMode = PlayerMode.Supra;
                    GameCore.gameModel.lineHeight = 20; 
                    GameCore.gameModel.linesProbability = new float[3] { 0.45f, 0.45f, 0.1f};
                    GameCore.gameModel.currLineHeight += 20;
                    GameCore.gameView.changeModeTrigger.transform.position = new Vector3(0.0f, GameCore.gameModel.currLineHeight, 0.0f);
                    GameCore.gameView.changeModeTrigger.gameObject.SetActive(true);
                    AddLines(10);
                    break;
                case 3:
                    GameCore.gameModel.lineHeight = 15;
                    AddLines(5);
                    break;
                case 4:
                    GameCore.gameView.playerView.playerMode = PlayerMode.LBias;
                    GameCore.gameModel.lineHeight = 20; 
                    GameCore.gameModel.linesProbability = new float[3] { 0.5f, 0.5f, 0.0f};
                    AddLines(10);
                    break;
                case 5:
                    GameCore.gameModel.lineHeight = 15; 
                    AddLines(5);
                    break;
                case 6:
                 //   GameCore.gameView.playerView.playerMode = PlayerMode.RBias;
                    GameCore.gameModel.lineHeight = 20; 
                    AddLines(5);
                    break;
                case 7:                    
                    GameCore.gameModel.lineHeight = 15; 
                    AddLines(5);
                    break;
            }                                                                                                       
        }                
    }
    
    public void AddLines(int num)
    {
        for(int i = 0; i < num; i++)
        {
            switch (ChooseProbability(GameCore.gameModel.linesProbability))
            {
                case 0:  
                    AddDrabLine();                  
                    break;
                case 1:           
                    AddColorLine();         
                    break;
                case 2:               
                    if(Random.Range(0, 2) == 0)
                        AddDrabLineIdle();
                    else
                        AddColorLineIdle();
                break;                
            }     
        }      
    }
    
    public Color[] SetLineColor(SpriteRenderer[] elements, Color[] colors)
    {    
        Color[] twoColors = new Color[2];
        
        List<Color> availableColors = new List<Color>();
        availableColors.AddRange(GameCore.gameModel.lineElementsColor);
        
        if(colors == null)
        {
            twoColors = GetRandomColors();
            elements[2].color = twoColors[0];
            elements[3].color = twoColors[1];
            elements[6].color = twoColors[0];
            elements[7].color = twoColors[1];
            elements[10].color = twoColors[0];
            elements[11].color = twoColors[1];
            elements[13].color = twoColors[0];
            elements[14].color = twoColors[1];
            elements[17].color = twoColors[0];
            elements[18].color = twoColors[1];
            
            availableColors.Remove(twoColors[0]);
            availableColors.Remove(twoColors[1]);   
        }        
        else
        {
            twoColors = colors;
            elements[2].color = colors[0];
            elements[3].color = colors[1];
            elements[6].color = colors[0];
            elements[7].color = colors[1];
            elements[10].color = colors[0];
            elements[11].color = colors[1];
            elements[13].color = colors[0];
            elements[14].color = colors[1];
            elements[17].color = colors[0];
            elements[18].color = colors[1];  
            
            availableColors.Remove(colors[0]);
            availableColors.Remove(colors[1]); 
        }
            
        for (int i = 0; i < elements.Length; i++)
        {
            if(elements[i].color == Color.white)
            {
                elements[i].color = availableColors[Random.Range(0, availableColors.Count)];
            }
        }
        return twoColors;
    }
    
    public Color[] SetLineColorIdle(SpriteRenderer[] elements)
    {    
        Color[] twoColors = GetRandomColors();
        
        elements[0].color = twoColors[0];
        elements[1].color = GameCore.gameModel.lineElementsColor[Random.Range(0, GameCore.gameModel.lineElementsColor.Length)];
        elements[2].color = twoColors[1];        
                    
        return twoColors;
    }
    
    public bool ChangePlayersDistance (float leftX, float leftY, float rightX, float rightY)
    {
        Transform left = GameCore.gameView.playerView.leftPlayerTransfrom;
        Transform right = GameCore.gameView.playerView.rightPlayerTransfrom;
        if(!Mathf.Approximately(left.position.x, leftX))
        {
            left.position = Vector3.MoveTowards(left.position, new Vector3 (leftX, leftY, 0.0f), Time.deltaTime * 15.0f);
            right.position = Vector3.MoveTowards(right.position, new Vector3 (rightX, rightY, 0.0f), Time.deltaTime * 15.0f);
            return false;
        }        
        return true;
    } 
    
    public void SetPlayerColor(Color[] colors)
    {
        GameCore.gameView.playerView.leftPlayerSpriteRenderer.color = colors[0];
        GameCore.gameView.playerView.rightPlayerSpriteRenderer.color = colors[1];
    }
    
    private void startLines_1()
    {
        AddDrabLineIdle();
        AddDrabLineToLeft(0.0f);
        AddDrabLineToRight(0.0f);
        AddDrabLineToRight(0.0f);
        AddDrabLineIdle();
        AddColorLineToRight(0.0f);
        AddColorLineToRight(0.0f);
        AddColorLineToLeft(0.0f);
        AddDrabLineIdle();
        AddColorLineToLeft(0.0f);
        AddColorLineToRight(0.0f);
        AddColorLineIdle();          
        AddColorLineToRight(0.0f);
        AddDrabLineToLeft(0.0f);
        AddDrabLineIdle();
    }
    
    private void startLines_2()
    {
        AddDrabLineIdle();
        AddDrabLineToLeft(0.0f);
        AddDrabLineToRight(0.0f);        
        AddDrabLineIdle();
        AddColorLineToLeft(0.0f);
        AddColorLineToRight(0.0f);
        AddColorLineToLeft(0.0f);
        AddDrabLineIdle();
        AddColorLineToLeft(0.0f);
        AddColorLineToRight(0.0f);
        AddColorLineIdle();          
        AddDrabLineToLeft(0.0f);
        AddColorLineToRight(0.0f);
        AddColorLineIdle();       
    }
    
    private void startLines_3()
    {
        AddDrabLineIdle();
        AddDrabLineToRight(0.0f);
        AddDrabLineToLeft(0.0f);
        AddDrabLineToLeft(0.0f);
        AddDrabLineIdle();
        AddColorLineToLeft(0.0f);
        AddColorLineToRight(0.0f);
        AddDrabLineIdle();
        AddColorLineToLeft(0.0f);
        AddColorLineToRight(0.0f);
        AddColorLineIdle();      
        AddColorLineIdle();
        AddDrabLineIdle();
        AddColorLineToLeft(0.0f);
    }        
    
    private int ChooseProbability(float[] probs)
    {
        int result = 0;
        float rand = UnityEngine.Random.value;                    
        float temp = 0;
        for (int i = 0; i < probs.Length; i++)
        {
            temp += probs[i];

            if (rand <= temp)
            {
                result = i;
                break;
            }
        }            
        return result;
    }
    
    private Color[] GetRandomColors()
    {
        Color[] colors = new Color[2];
        switch (Random.Range(0, 16))
        {
            case 0:                
                colors[0] = GameCore.gameModel.lineElementsColor[0];
                colors[1] = GameCore.gameModel.lineElementsColor[0];
            break;            
            case 1:
                colors[0] = GameCore.gameModel.lineElementsColor[1];
                colors[1] = GameCore.gameModel.lineElementsColor[1];
            break;
            case 2:
                colors[0] = GameCore.gameModel.lineElementsColor[2];
                colors[1] = GameCore.gameModel.lineElementsColor[2];
            break;
            case 3:
                colors[0] = GameCore.gameModel.lineElementsColor[3];
                colors[1] = GameCore.gameModel.lineElementsColor[3];
            break;
            case 4:
                colors[0] = GameCore.gameModel.lineElementsColor[0];
                colors[1] = GameCore.gameModel.lineElementsColor[1];
            break;
            case 5:
                colors[0] = GameCore.gameModel.lineElementsColor[0];
                colors[1] = GameCore.gameModel.lineElementsColor[2];
            break;
            case 6:
                colors[0] = GameCore.gameModel.lineElementsColor[0];
                colors[1] = GameCore.gameModel.lineElementsColor[3];
            break;
            case 7:
                colors[0] = GameCore.gameModel.lineElementsColor[1];
                colors[1] = GameCore.gameModel.lineElementsColor[0];
            break;
            case 8:
                colors[0] = GameCore.gameModel.lineElementsColor[1];
                colors[1] = GameCore.gameModel.lineElementsColor[2];
            break;
            case 9:
                colors[0] = GameCore.gameModel.lineElementsColor[1];
                colors[1] = GameCore.gameModel.lineElementsColor[3];
            break;
            case 10:
                colors[0] = GameCore.gameModel.lineElementsColor[2];
                colors[1] = GameCore.gameModel.lineElementsColor[0];
            break;
            case 11:
                colors[0] = GameCore.gameModel.lineElementsColor[2];
                colors[1] = GameCore.gameModel.lineElementsColor[1];
            break;
            case 12:
                colors[0] = GameCore.gameModel.lineElementsColor[2];
                colors[1] = GameCore.gameModel.lineElementsColor[3];
            break;
            case 13:
                colors[0] = GameCore.gameModel.lineElementsColor[3];
                colors[1] = GameCore.gameModel.lineElementsColor[0];
            break;
            case 14:
                colors[0] = GameCore.gameModel.lineElementsColor[3];
                colors[1] = GameCore.gameModel.lineElementsColor[1];
            break;
            case 15:
                colors[0] = GameCore.gameModel.lineElementsColor[3];
                colors[1] = GameCore.gameModel.lineElementsColor[2];
            break;
        }
        return colors;
    }
}
