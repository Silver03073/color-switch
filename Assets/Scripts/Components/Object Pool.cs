﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Object Pool Manager Created by Pavliuk Serhiy
/// </summary>
public class ObjectPool
{
    private string _folderName; //name of the folder in which the objects are that we want to put in a pool
    private GameObject[] _folderObjects; //objects that we want to put in a pool
    private List<GameObject> _loadedObjects; //objects loaded into memory

    public int Count
    {
        get
        {
            return _loadedObjects.Count;
        }
    }

    public ObjectPool (string folderName)
    {
        this._folderName = folderName;
        _folderObjects = Resources.LoadAll<GameObject> (_folderName);
    }

    public void LoadObjects (int numOfClones) //loading objects into memory
    {
        if(_folderObjects == null)
        {
            Debug.LogError ("Unable to load objects from folder");
            return;
        }
        _loadedObjects = new List<GameObject> ();
        for (int i = 0; i < _folderObjects.Length; i++)
        {
            for(int n = 0; n < numOfClones; n++)
            {
                GameObject temp = GameObject.Instantiate(_folderObjects[i]) as GameObject;
                temp.SetActive(false);
                _loadedObjects.Add(temp);
            }            
        }
    }

    public GameObject GetPooledObject () //give an object from the pool
    {
        int rand = Random.Range(0, _loadedObjects.Count);
        if(_loadedObjects[rand].activeInHierarchy == false)
            return _loadedObjects[rand];
        else
        {
            foreach(GameObject pooledObject in _loadedObjects)
            {
                if(pooledObject.activeInHierarchy == false)
                {
                    return pooledObject;
                }
            }   
        }            
        return GetPooledCloneObjectAt(rand);
    }
    
    public GameObject GetPooledObjectAt (int index) //give an object from the pool at index
    {
        if(_loadedObjects[index].activeInHierarchy == false)
        {
            return _loadedObjects[index];
        }
        return GetPooledCloneObjectAt(index);
    }
    
    public GameObject GetPooledObjectSegment (int a, int b) //give an object from the pool at index
    {
        for ( int i = a; i < b; i++ )
        {
            if(_loadedObjects[i].activeInHierarchy == false)
            {
                return _loadedObjects[i];
            }
        }        
        return GetPooledCloneObjectAt(a);
    }
    
    public GameObject GetPooledCloneObjectAt (int index)
    {
        GameObject cloneObject = GameObject.Instantiate (_loadedObjects [index]) as GameObject;
        cloneObject.SetActive(false);
        return cloneObject;
    }

    public void DeactivateObject (GameObject gameObject)
    {
        if(gameObject != null)
        {
            int index = _loadedObjects.IndexOf (gameObject);
            if (index >= 0 && index <= _loadedObjects.Count)
            {
                _loadedObjects [index].SetActive(false);
            }
            else
            {
                GameObject.Destroy (gameObject);
            }
        }
    }

    public void UnloadObjects () //unloading objects into memory
    {
        foreach (GameObject pooledObject in _loadedObjects)
        {
            GameObject.Destroy(pooledObject);
        }
    }
}