﻿using UnityEngine;

public class DownUpAnim : MonoBehaviour 
{
    public float up = 0.5f;
    public float down = -0.5f;    
    public float speed = 4.0f;
	
    void Start()
    {
        transform.position = new Vector3(transform.position.x, down, transform.position.z);
    }
    
	// Update is called once per frame
	void Update () 
    {
	   DownUpDownLoop();
	}
    
    private void DownUpDownLoop ()
    {
        if(Mathf.Approximately(transform.position.y, down))
        {
            do
            {
                transform.position = Vector3.MoveTowards(transform.position, new Vector3 (transform.position.x, up, transform.position.z), Time.deltaTime * speed);
            } while (!Mathf.Approximately(transform.position.y, up));
            do
            {
                transform.position = Vector3.MoveTowards(transform.position, new Vector3 (transform.position.x, down, transform.position.z), Time.deltaTime * speed);
            } while (!Mathf.Approximately(transform.position.y, down));                           
        }
    }        
}

