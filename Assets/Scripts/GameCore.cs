﻿using System;
using System.Collections;
using UnityEngine;

public class GameCore : MonoBehaviour 
{    
    public static GameCore Instance { get; set; }
    public GameModel gameModel;
    public GameController gameController;
    public GameView gameView;  
    
    void Awake()
    {
        Instance = this;
    }
    
    public T[] GetComponentsInChildrenCustom<T> (GameObject parent)
    {
        return parent.GetComponentsInChildren<T>();
    }  
    
    public IEnumerator WaitThenCallback(float time, Action callback)
    {
        yield return new WaitForSeconds(time);
        callback();
    }
 
}
