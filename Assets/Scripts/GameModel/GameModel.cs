﻿using UnityEngine;
using System.Collections.Generic;

public class GameModel : GameComponent 
{
    public Vector3 gravity = new Vector3(0.0f, -200.0f, 0.0f);
    public Vector3 flapVelocity = new Vector3(0.0f, 200.0f, 0.0f);
    public float flapSpeed = 35;
    [HideInInspector]    
    public bool isPlaying;
    [HideInInspector]  
    public bool isFirstTap;   
    public ObjectPool LinesPool { get; set; }
    public int lineSpeed = 10;
    public int lineHeight = 15;
    [HideInInspector]
    public int currLineHeight = 0;
    public Color[] lineElementsColor;
    public int numberOfLineClones = 5;
    [HideInInspector]
    public float[] linesProbability = new float[3] { 0.4f, 0.2f, 0.4f};
    [HideInInspector]
    public int modeCount = 0; 
}
