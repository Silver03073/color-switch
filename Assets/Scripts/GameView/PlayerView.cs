﻿using UnityEngine;

public enum PlayerMode
{
    Beside,
    Supra,
    LBias,
    RBias
}

public class PlayerView : GameComponent 
{
    public PlayerMode playerMode;
    [HideInInspector]
    public Rigidbody2D _rigidbody2D;
    [HideInInspector]
    public Transform _transform;    
    public Transform leftPlayerTransfrom;
    public Transform rightPlayerTransfrom;    
    public SpriteRenderer leftPlayerSpriteRenderer;
    public SpriteRenderer rightPlayerSpriteRenderer; 
    
    void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _transform = GetComponent<Transform>();
    }      
    
    void Update()
    {       
       GameCore.gameController.Bounce(); 
       StartCoroutine(GameCore.gameController.DetectBounceFail());
    }    
}
