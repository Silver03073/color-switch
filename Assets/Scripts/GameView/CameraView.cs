﻿using UnityEngine;
using System.Collections;

public class CameraView : GameComponent 
{
    [HideInInspector]
    public Transform _transform;
    [HideInInspector]
    public float topPos;
    
    void Awake()
    {
        _transform = GetComponent<Transform>();
    }    
    
    void Start()
    {
        GameCore.gameController.CameraInit();
    }
    
	void LateUpdate () 
    {
        GameCore.gameController.CameraFollow();
    }
}
