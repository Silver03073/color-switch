﻿using UnityEngine;

public class LineBaseView : GameComponent 
{
    protected bool isSupported;
    protected bool isHitWithPlayer = false;                  
    
    protected virtual void OnEnable()
    {
        isSupported = false;        
    }
	
    protected virtual void OnDisable()
    {
        CancelInvoke();
    }
    
    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            isHitWithPlayer = true;
        }        
    } 
    
    protected void DeactivationLineWrap()
    {
        GameCore.gameController.DeactivationLine(this.transform);   
    }   
}
