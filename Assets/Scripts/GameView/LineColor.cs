﻿using UnityEngine;

public class LineColor : LineBaseView 
{
    public SpriteRenderer[] elements;       
    public Color[] colors = new Color[2];    
    
    private void Awake()
    {
        elements = GameCore.GetComponentsInChildrenCustom<SpriteRenderer>(this.gameObject);
    } 
    
	protected override void OnEnable()
    {
        base.OnEnable();
        InvokeRepeating("DeactivationLineWrap", 1.0f, 1.0f);
        InvokeRepeating("SupportColorLineWrap", 6.0f, 2.0f);
    }  
    
    protected override void OnDisable()
    {
        base.OnDisable();
        for(int i = 0; i < elements.Length; i++)
        {
            elements[i].color = Color.white;
        }
    }       
	
    protected override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);
        
        if(other.gameObject.CompareTag("Player"))
        {
            GameCore.gameController.SetPlayerColor(colors);
        }    
    }
    
	// Update is called once per frame
	void Update () 
    {        
       if(GameCore.gameController.IsLineVisible(this.transform))
       {
            GameCore.gameController.MoveLine(this.transform);   
                     
            if (isHitWithPlayer == true)
            {
                if(GameCore.gameView.playerView.playerMode == PlayerMode.Beside)
                {
                    if(GameCore.gameController.ChangePlayersDistance(-5.0f, GameCore.gameView.playerView.leftPlayerTransfrom.position.y, 5.0f, GameCore.gameView.playerView.leftPlayerTransfrom.position.y) == true)
                        isHitWithPlayer = false;   
                }                
            }
       }       
	} 
    
    private void SupportColorLineWrap()
    {
        if(isSupported == false)
        {
            if(GameCore.gameController.SupportColorLine(this.transform, colors) == true)
                isSupported = true;
        }  
    }   
}
