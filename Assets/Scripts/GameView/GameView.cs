﻿using UnityEngine;
using System.Collections;

public class GameView : GameComponent 
{
    [HideInInspector]
    public Transform _transform;
    public Gameplay gameplay;
    public PlayerView playerView;  
    public CameraView cameraView;
    public LineBaseView lineBaseView;
    public CMTView changeModeTrigger;
    
    void Awake()
    {
        _transform = GetComponent<Transform>();
    }        
}
