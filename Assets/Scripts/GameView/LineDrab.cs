﻿public class LineDrab : LineBaseView
{
    protected override void OnEnable()
    {
        base.OnEnable();
        InvokeRepeating("DeactivationLineWrap", 2.0f, 2.0f);
        InvokeRepeating("SupportColorLineWrap", 6.0f, 2.0f);
    }
	
    protected override void OnDisable()
    {
        base.OnDisable();
    } 
    
	// Update is called once per frame
    void Update () 
    {
       if(GameCore.gameController.IsLineVisible(this.transform))
       {
            GameCore.gameController.MoveLine(this.transform);       
                 
            if (isHitWithPlayer == true)
            {
                if(GameCore.gameView.playerView.playerMode == PlayerMode.Beside)
                {
                    if(GameCore.gameController.ChangePlayersDistance(-9.95f, GameCore.gameView.playerView.leftPlayerTransfrom.position.y, 9.95f, GameCore.gameView.playerView.leftPlayerTransfrom.position.y) == true)
                        isHitWithPlayer = false;   
                }                
            }   
       }       
	}
    
    private void SupportColorLineWrap()
    {
        if(isSupported == false)
        {
            if(GameCore.gameController.SupportDrabLine(this.transform) == true)
                isSupported = true;
        }  
    }  
}
