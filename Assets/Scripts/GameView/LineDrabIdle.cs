﻿public class LineDrabIdle : LineBaseView 
{	    
    protected override void OnEnable()
    {
        base.OnEnable();
        InvokeRepeating("DeactivationLineWrap", 8.0f, 8.0f);
    }  
    
    protected override void OnDisable()
    {
        base.OnDisable();
    }    
                
	// Update is called once per frame
	void Update () 
    {            
       if (isHitWithPlayer == true)
       {
            if(GameCore.gameView.playerView.playerMode == PlayerMode.Beside)
            {
                    if(GameCore.gameController.ChangePlayersDistance(0.0f, GameCore.gameView.playerView.leftPlayerTransfrom.position.y, 0.0f, GameCore.gameView.playerView.leftPlayerTransfrom.position.y) == true)
                        isHitWithPlayer = false;
            }            
       }
	}
}
