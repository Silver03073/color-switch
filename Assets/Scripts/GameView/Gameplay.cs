﻿public class Gameplay : GameComponent 
{            
    void Awake()
    {
        GameCore.gameController.LoadLinesToPool();            
    }
    
	// Use this for initialization	
    void Start () 
    {
        GameCore.gameController.StartNewGame();
	}
	
	// Update is called once per frame
	void Update () 
    {
        GameCore.gameController.MouseClick();
        GameCore.gameController.LineControl();
	}
}
