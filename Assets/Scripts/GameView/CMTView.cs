﻿using UnityEngine;

public class CMTView : GameComponent 
{
    private bool isHitWithPlayer = false;    
	private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            isHitWithPlayer = true;
        }        
    }
	
	// Update is called once per frame
	void Update () 
    {
        if (isHitWithPlayer == true)
        {
            if(GameCore.gameController.ChangePlayersDistance(0.1f, 6.0f, -0.1f, -6.0f) == true)
                isHitWithPlayer = false;  
        }
	}
}
