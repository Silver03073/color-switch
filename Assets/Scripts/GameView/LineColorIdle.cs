﻿using UnityEngine;

public class LineColorIdle : LineBaseView 
{	
    public SpriteRenderer[] elements;  
    
    public Color[] colors = new Color[2];      
    
    private void Awake()
    {
        elements = GameCore.GetComponentsInChildrenCustom<SpriteRenderer>(this.gameObject);
    } 
    
	protected override void OnEnable()
    {
        base.OnEnable();
        InvokeRepeating("DeactivationLineWrap", 8.0f, 8.0f);
    }     
    
    protected override void OnDisable()
    {
        base.OnDisable();
        for(int i = 0; i < elements.Length; i++)
        {
            elements[i].color = Color.white;
        }
    } 
    
    protected override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);
        
        if(other.gameObject.CompareTag("Player"))
        {
            GameCore.gameController.SetPlayerColor(colors);
        }    
    }
    
	// Update is called once per frame
	void Update () 
    {  
       if (isHitWithPlayer == true)
       {
            if(GameCore.gameView.playerView.playerMode == PlayerMode.Beside)
            {
                    if(GameCore.gameController.ChangePlayersDistance(-11.0f, GameCore.gameView.playerView.leftPlayerTransfrom.position.y, 11.0f, GameCore.gameView.playerView.leftPlayerTransfrom.position.y) == true)
                        isHitWithPlayer = false;   
            }             
       }     
	}
}
